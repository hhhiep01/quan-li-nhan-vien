﻿using System;
using System.Text.Json;

public class EmployeeManagementUI
{
    string filePath = "C:\\Net04\\employee-management-master (1)\\employee-management-master\\EmployeeManagement\\EmployeeList.json";
    public EmployeeManagement employeeManagement { get; set; }

    public EmployeeManagementUI()
    {
        // đối tượng này dùng để quản lý và lưu trữ nhân viên
        employeeManagement = new EmployeeManagement();
    }

    public void PrintEmployees()
    {
        try
        {
            Console.WriteLine("Hiển thị thông tin của nhân viên");
            Employee[] employees = employeeManagement.GetEmployees();
            if (employees != null)
            {
                foreach (Employee employee in employees)
                {

                    if (employee != null)
                    {
                        Console.WriteLine($"Tên: [{employee.Name}], Tuổi: [{employee.Age}], Chức vụ: [{employee.Position}], Lương: [{employee.Salary}]");
                    }
                    else
                    {
                        Console.WriteLine("chưa có nhân viên");
                        return;
                    }

                }
            }
            else
            {
                throw new NullReferenceException("mang rong");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Message: {ex.Message}");
        }


        // TODO: Hiển thị thông tin của nhân viên
        // Định dạng in ra: Tên: [name], Tuổi: [age], Chức vụ: [position], Lương: [salary]

    }

    public void AddEmployee()
    {
        try
        {

            // TODO: Yêu cầu người dùng nhập tên, tuổi, chức vụ và lương của nhân viên mới
            Console.WriteLine("Tạo một đối tượng Employee và thêm vào danh sách");
            Console.Write("Nhập tên nhân viên: ");
            string name = Console.ReadLine();
            Console.Write("Nhập tuổi nhân viên: ");
            int age = int.Parse(Console.ReadLine());
            Console.Write("Nhập vị trí nhân viên: ");
            string position = Console.ReadLine();
            Console.Write("Nhập lương nhân viên: ");
            decimal salary = decimal.Parse(Console.ReadLine());
            employeeManagement.AddEmployee(name, age, position, salary);
            SaveToFile();
            Console.WriteLine("Thêm thành công");
        }
        catch (AggregateException ex)
        {
            Console.WriteLine($"Message: {ex.Message}");
        }

        // Tạo một đối tượng Employee và gọi hàm employeeManagement.AddEmployee();
    }

    public void RemoveEmployee()
    {
        // TODO: Yêu cầu người dùng nhập tên của nhân viên cần xóa
        // gọi hàm employeeManagement.RemoveEmployee("hehehee");
        Console.WriteLine("Tìm và loại bỏ nhân viên có tên trùng khớp từ danh sách");
        PrintEmployees();
        Console.Write("Nhập tên nhân viên cần xóa: ");
        string name = Console.ReadLine();
        employeeManagement.RemoveEmployee(name);
        Console.WriteLine($"Xóa thành công {name}");


    }
    public void SaveToFile()
    {
        Employee[] employees = employeeManagement.GetEmployees();
        List<Employee> listEmployee = new List<Employee>();

        if (File.Exists(filePath))
        {

            string fileContent = File.ReadAllText(filePath);
            if (!string.IsNullOrWhiteSpace(fileContent))
            {

                listEmployee = JsonSerializer.Deserialize<List<Employee>>(fileContent);
            }
        }

        foreach (Employee emp in employees)
        {
            if (emp != null)
            {
                listEmployee.Add(emp);
            }
            else
            {
                break;
            }

        }

        // Serialize the updated list to JSON
        string json = JsonSerializer.Serialize(listEmployee);

        // Write the JSON back to the file
        File.WriteAllText(filePath, json);
    }


    public void LoadFormFile()
    {
        try
        {
            if (File.Exists(filePath))
            {
                string fileContentJson = File.ReadAllText(filePath);
                if (!string.IsNullOrWhiteSpace(fileContentJson))
                {
                    List<Employee> listEmployee = JsonSerializer.Deserialize<List<Employee>>(fileContentJson);

                    if (listEmployee != null)
                    {
                        for (int i = 0; i < listEmployee.Count; i++)
                        {
                            if (listEmployee[i] != null)
                            {
                                Console.WriteLine($"Tên: [{listEmployee[i].Name}], Tuổi: [{listEmployee[i].Age}], Chức vụ: [{listEmployee[i].Position}], Lương: [{listEmployee[i].Salary}]");
                            }

                        }
                    }
                }
                else
                {
                    throw new NullReferenceException("file rong");
                }

            }
        }
        catch (NullReferenceException ex)
        {
            Console.WriteLine(ex.Message);
        }

    }


}

