﻿using System.Text.Json;
using System;

public class EmployeeManagement
{
    Employee[] employees { get; set; }
    int count = 0;
    public EmployeeManagement()
    {
        employees = new Employee[100]; // Số nhân viên lưu trữ tối đa
    }

    public Employee[] GetEmployees()
    {   

        // TODO: trả về thông tin của nhân viên trong hệ thống
        return employees;
    }

    public void AddEmployee(string name, int age, string possition, decimal salary)
    {
        employees[count++] = new Employee()
        {
            Id = count,
            Name = name,
            Age = age,
            Position = possition,
            Salary = salary
        };

        // TODO: Viết logic để thêm nhân viên mới vào employees nhớ phải phân biệt nhau bằng ID
    }

    public void RemoveEmployee(string name)
    {

        // TODO: Viết logic xóa nhân viên với tên
        try
        {
            for (int i = 0; i < employees.Length; i++)
            {
                if (employees[i] == null)
                {
                    throw new NullReferenceException("Null");
                }
                else if (employees[i].Name == name)
                {
                    employees[i] = null;
                    return;
                }
                else
                {
                    continue;
                }
            }
        }catch (NullReferenceException ex)
        {
            Console.WriteLine(ex.ToString());
        }
        


    }


}
